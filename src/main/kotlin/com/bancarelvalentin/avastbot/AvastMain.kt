package com.bancarelvalentin.avastbot

import com.bancarelvalentin.ezbot.Orchestrator
import com.bancarelvalentin.ezbot.config.ConfigHandler
import com.bancarelvalentin.glrcbot.common.CommonUtils
import com.bancarelvalentin.glrcbot.common.GlrcLiveConfig

// https://discord.com/oauth2/authorize?client_id=777006882476916746&scope=bot&permissions=8
class AvastMain {
    companion object {
        
        @JvmStatic
        fun main(args: Array<String>) {
            ConfigHandler.config = AvastCommonConfig()
            ConfigHandler.liveConfig = GlrcLiveConfig()
            CommonUtils.addAllProcessToOrchestrator()
            Orchestrator.start()
        }
    }
}
