package com.bancarelvalentin.avastbot

import com.bancarelvalentin.glrcbot.common.CommonConfig
import com.bancarelvalentin.glrcbot.common.IDENTITY

/**
 * Config for avast bot required for the use in parallel with truffe bot
 */
@Suppress("unused")
class AvastCommonConfig : CommonConfig(IDENTITY.AVAST) {
    
    override val prefixes = arrayOf("a.", "\\", *commonPrefixes)
    override val defaultColorHex = "#292929"
    override val supportLazyCommands = false
}